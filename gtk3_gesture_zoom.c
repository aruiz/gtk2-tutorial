#include <gtk/gtk.h>
#include <math.h>

static void
on_draw (GtkWidget *widget, cairo_t *ctx, gpointer data)
{
    GtkGesture *zoom = data;

    cairo_matrix_t matrix;
    gdouble x_center, y_center, scale;

    if (gtk_gesture_is_recognized (zoom)) {
        gtk_gesture_get_bounding_box_center (zoom, &x_center, &y_center);
        cairo_get_matrix (ctx, &matrix);
        cairo_matrix_translate (&matrix, x_center, y_center);

        scale = gtk_gesture_zoom_get_scale_delta (GTK_GESTURE_ZOOM (zoom));
        cairo_matrix_scale (&matrix, scale, scale);
        cairo_set_matrix (ctx, &matrix);
    }

    cairo_set_source_rgb (ctx, 0, 0, 0);
    cairo_paint (ctx);

    cairo_set_source_rgb (ctx, 1, 1, 1);
    cairo_rectangle (ctx, 50, 50, 350, 350);
    cairo_fill (ctx);
}

void
zoom_scale_changed_cb (GtkGestureZoom *zoom,
                       gdouble         scale,
                       GtkDrawingArea *widget) {
    gtk_widget_queue_draw (GTK_WIDGET (widget));
}

gint
main (gint argc, gchar **argv)
{
    gtk_init (&argc, &argv);

    GtkWidget *win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    GtkWidget *area = gtk_drawing_area_new ();

    gtk_container_add (GTK_CONTAINER (win), area);
    gtk_widget_show_all (win);

    gtk_widget_add_events (area, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK);

    GtkGesture *zoom = gtk_gesture_zoom_new (GTK_WIDGET (area));

    g_signal_connect (area, "draw", G_CALLBACK (on_draw), zoom);

    g_signal_connect (zoom, "scale-changed",
                    G_CALLBACK (zoom_scale_changed_cb), area);

    gtk_main ();
    return 0;
}
