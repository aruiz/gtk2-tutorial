#include <gtk/gtk.h>
#include <math.h>

typedef struct {
    double x;
    double y;
    double width;
    double height;
    double x_press;
    double y_press;
} Coordinates;

static void
on_expose (GtkWidget *widget, GdkEvent *expose_event, gpointer data)
{
    Coordinates *coords = (Coordinates*)data;

    cairo_t *ctx = gdk_cairo_create (GDK_DRAWABLE (gtk_widget_get_window (widget)));

    cairo_set_source_rgb (ctx, 0, 0, 0);
    cairo_paint (ctx);    

    cairo_set_source_rgb (ctx, 1, 1, 1);
    cairo_rectangle (ctx, coords->x, coords->y, coords->width, coords->height);
    cairo_fill (ctx);

    cairo_destroy (ctx);
}

static void
on_press (GtkWidget *area, GdkEventButton *event, gpointer data)
{
    g_message ("press: button=%d x=%f y=%f", event->button, event->x, event->y);
    Coordinates *coords = (Coordinates*)data;
    coords->x_press = event->x;
    coords->y_press = event->y;
}

static void
on_release (GtkWidget *area, GdkEventButton *event, gpointer data)
{
    g_message ("release: button=%d x=%f y=%f", event->button, event->x, event->y);
    Coordinates *coords = (Coordinates*)data;

    coords->x_press = -1;
    coords->y_press = -1;
}

static void
on_motion (GtkWidget *area, GdkEventMotion *event, gpointer data)
{
    Coordinates *coords = (Coordinates*)data;

    if (coords->x_press == -1 && coords->y_press == -1)
        return;

    if (coords->x_press <= event->x)
        coords->x = coords->x_press;
    else
        coords->x = event->x;

    if (coords->y_press <= event->y)
        coords->y = coords->y_press;
    else
        coords->y = event->y;

    coords->width = fabs(coords->x_press - event->x);
    coords->height = fabs(coords->y_press - event->y);

    gtk_widget_queue_draw (area); // Forces a redraw of the widget
}

gint
main (gint argc, gchar **argv)
{
    gtk_init (&argc, &argv);

    GtkWidget *win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    GtkWidget *area = gtk_drawing_area_new ();

    gtk_container_add (GTK_CONTAINER (win), area);
    gtk_widget_show_all (win);

    gtk_widget_add_events (area, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK);

    Coordinates coords = {0,0,0,0,-1,-1};

    g_signal_connect (area, "expose-event", G_CALLBACK (on_expose), &coords);
    g_signal_connect (area, "button-press-event", G_CALLBACK (on_press), &coords);
    g_signal_connect (area, "button-release-event", G_CALLBACK (on_release), &coords);
    g_signal_connect (area, "motion-notify-event", G_CALLBACK (on_motion), &coords);

    gtk_main ();
    return 0;
}