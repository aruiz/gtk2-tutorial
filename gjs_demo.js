const Lang = imports.lang;

imports.gi.versions.Gtk = '3.0'
const Gtk = imports.gi.Gtk;

Gtk.init(null);
let win = Gtk.Window.new(Gtk.WindowType.TOPLEVEL);
let button = Gtk.Button.new_with_label("Click me!");
win.add(button);
win.show_all();

win.connect("destroy", function (window, event) { Gtk.main_quit() });
button.connect("clicked", function (button) { print("Hello World!") });

Gtk.main();