#include <gtk/gtk.h>
#include <math.h>

static void
on_expose (GtkWidget *widget, GdkEvent *expose_event, gpointer data)
{
    cairo_t *ctx = gdk_cairo_create (GDK_DRAWABLE (gtk_widget_get_window (widget)));

    PangoLayout  *layout = pango_cairo_create_layout (ctx);

    pango_layout_set_text (layout, "Hello Pango!", -1);
    PangoFontDescription *desc = pango_font_description_from_string ("Sans Bold 27");

    pango_cairo_show_layout (ctx, layout);

    pango_font_description_free (desc);
    g_object_unref (layout);    
    cairo_destroy (ctx);
}

gint
main (gint argc, gchar **argv)
{
    gtk_init (&argc, &argv);

    GtkWidget *win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    GtkWidget *area = gtk_drawing_area_new ();

    gtk_container_add (GTK_CONTAINER (win), area);
    gtk_widget_show_all (win);

    g_signal_connect (area, "expose-event", G_CALLBACK (on_expose), NULL);

    gtk_main ();
    return 0;
}