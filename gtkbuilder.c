#include <gtk/gtk.h>

static void
on_clicked (GtkButton *button, gpointer entry)
{
    g_message("%s", gtk_entry_get_text (GTK_ENTRY (entry)));
}

gint
main (gint argc, gchar **argv)
{
    gtk_init (&argc, &argv);

    GtkBuilder *bldr = gtk_builder_new();
    gtk_builder_add_from_file (bldr, "builder.ui", NULL);

    GObject *window1 = gtk_builder_get_object (bldr, "window1");
    gtk_widget_show_all (GTK_WIDGET (window1));

    GObject *entry1  = gtk_builder_get_object (bldr, "entry1");
    GObject *button1 = gtk_builder_get_object (bldr, "button1");

    g_signal_connect (button1, "clicked", G_CALLBACK (on_clicked), entry1);

    gtk_main ();
    return 0;
}