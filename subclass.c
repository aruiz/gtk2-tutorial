#include "subclass.h"
#include <gio/gio.h>

typedef struct {
    GFile* file; // File abstraction in GIO
} KndlSubclassPrivate;

G_DEFINE_TYPE_WITH_PRIVATE(KndlSubclass, kndl_subclass, G_TYPE_OBJECT)

//Properties' ids
enum
{
  PROP_FILE = 1,
  N_PROPERTIES
};

// Signal ids
enum {
  PINGED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = {0,}; // static signal array

// Property setter virtual function for KndlSubclass
static void
kndl_subclass_set_property (GObject      *self,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec) {
    KndlSubclassPrivate *priv = kndl_subclass_get_instance_private (KNDL_SUBCLASS(self));

    switch (prop_id) {
        case PROP_FILE:
            g_clear_object (&priv->file); // unrefs the object and sets the pointer to 0
            priv->file = G_FILE(g_value_get_object(value));
            g_object_ref (priv->file);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (self, prop_id, pspec);
            break;
    }
}

static void
kndl_subclass_get_property (GObject    *self,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec) {
    KndlSubclassPrivate *priv = kndl_subclass_get_instance_private (KNDL_SUBCLASS(self));

    switch (prop_id) {
        case PROP_FILE:
            g_value_set_object(value, priv->file);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (self, prop_id, pspec);
            break;
    }
}

static void
kndl_subclass_dispose (GObject *self) {
    KndlSubclassPrivate *priv = kndl_subclass_get_instance_private (KNDL_SUBCLASS(self));
    
    g_message ("Disposing object with file: %s", g_file_get_path (priv->file));
    g_clear_object (&priv->file);

    G_OBJECT_CLASS (kndl_subclass_parent_class)->finalize (self); // Chain up to the parent class' finalize method
}

void
kndl_subclass_class_init (KndlSubclassClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_message ("KndlSubclass type initalization");

  object_class->set_property = kndl_subclass_set_property; // static property setter
  object_class->get_property = kndl_subclass_get_property; // static property getter

  signals[PINGED] =
    g_signal_new ("pinged",                           // name
                  G_OBJECT_CLASS_TYPE (klass),        // class
                  G_SIGNAL_RUN_LAST,                  // priority
                  G_STRUCT_OFFSET (KndlSubclassClass, pinged),
                  NULL, NULL,                         // signal accumulator & data
                  g_cclosure_marshal_VOID__INT,       // argument marshaller
                  G_TYPE_NONE,                        // return type
                  1,                                  // number of arguments
                  G_TYPE_INT);                        // 1st argument type

  g_object_class_install_property (object_class,
                                   PROP_FILE,
                                   g_param_spec_object ("file",
                                                        "File",
                                                        "A #GFile object that is a property of this class",
                                                        G_TYPE_FILE,
                                                        G_PARAM_READWRITE)); // read & write support

  object_class->dispose = kndl_subclass_dispose; // Override virtual function
}

void
kndl_subclass_init (KndlSubclass *self)
{
  KndlSubclassPrivate *priv = kndl_subclass_get_instance_private (self);
  priv->file = NULL;
}

KndlSubclass*
kndl_subclass_new (const gchar *msg) {
    GFile *file = g_file_new_for_path (msg);
    KndlSubclass* self = g_object_new(KNDL_TYPE_SUBCLASS, "file", file, NULL);
    g_object_unref (file);
    return self;
}

GFile*
kndl_subclass_get_file (KndlSubclass *self) {
    KndlSubclassPrivate *priv = kndl_subclass_get_instance_private (self);

    return priv->file;
}

void
kndl_subclass_set_file (KndlSubclass *self, GFile *file) {   
    g_object_set (G_OBJECT (self), "file", file, NULL);
}

void
kndl_subclass_ping (KndlSubclass *self, gint value) {
    g_signal_emit (self, signals[PINGED], 0, value);
}