#include <gtk/gtk.h>

static void
window_closed (GtkWidget *widget,
               GdkEvent  *event,
               gpointer   data) {
  gtk_main_quit ();
}

gint
main (gint argc, gchar **argv) {
  gtk_init (&argc, &argv);

  GtkWidget *window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (window, "delete-event", G_CALLBACK(window_closed), NULL);

  GtkWidget *vbox = gtk_vbox_new (TRUE, 0);

  gtk_container_add (GTK_CONTAINER(vbox), gtk_label_new("gtk_hbox_new (TRUE, 0);"));
  // Homogeneous Horizontal Boxes
  GtkWidget *hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("expand = true"), TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("fill = true"), TRUE, TRUE, 0);
  gtk_container_add(GTK_CONTAINER(vbox), hbox1);

  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("expand = true"), TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("fill = false"), TRUE, FALSE, 0);
  gtk_container_add(GTK_CONTAINER(vbox), hbox1);

  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("expand = false"), FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("fill = false"), FALSE, FALSE, 0);
  gtk_box_pack_end   (GTK_BOX(hbox1), gtk_button_new_with_label("pack_end"), FALSE, FALSE, 0);
  gtk_container_add(GTK_CONTAINER(vbox), hbox1);

  //////////////////////////////////////////////////////////////////////////////////////////////////
  gtk_container_add (GTK_CONTAINER(vbox), gtk_label_new("gtk_hbox_new (FALSE, 0);"));
  // Heterogeneous Horizontal Boxes
  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("expand = true"), TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("fill = true"), TRUE, TRUE, 0);
  gtk_container_add(GTK_CONTAINER(vbox), hbox1);

  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("expand = true"), TRUE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("fill = false"), TRUE, FALSE, 0);
  gtk_container_add(GTK_CONTAINER(vbox), hbox1);

  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("expand = false"), FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("fill = false"), FALSE, FALSE, 0);
  gtk_box_pack_end   (GTK_BOX(hbox1), gtk_button_new_with_label("pack_end"), FALSE, FALSE, 0);
  gtk_container_add(GTK_CONTAINER(vbox), hbox1);

  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("expand = false"), FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX(hbox1), gtk_button_new_with_label("fill = false"), FALSE, FALSE, 0);
  gtk_box_pack_end   (GTK_BOX(hbox1), gtk_button_new_with_label("pack_end"), FALSE, FALSE, 0);
  gtk_box_pack_end   (GTK_BOX(hbox1), gtk_button_new_with_label("fill & expand"), TRUE, TRUE, 0);
  gtk_container_add(GTK_CONTAINER(vbox), hbox1);

  gtk_container_add (GTK_CONTAINER(window), vbox);
  gtk_widget_show_all (window);

  gtk_main ();
  return 0;
}