namespace Kindle {
  public class Subclass : Object {
    public File file { get; set; }
    public signal void pinged (int value);

    public Subclass (string filename) {
      file = File.new_for_path (filename);
    }

    public void ping (int value) {
      pinged(value);
    }
  }
}

static int main (string[] args) {
  var ks = new Kindle.Subclass("1");
  ks.pinged.connect ((ks2, value) => {
    message("%s pinged: %d", ks2.file.get_path(), value);
  });

  Timeout.add(1000, () => {
    ks.ping(3000);
    return true;
  });

  var loop = new MainLoop(null, false);
  loop.run();
  return 0;
}