#ifndef _KNDL_SUBCLASS_H_
#define _KNDL_SUBCLASS_H_

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

// C does not support types as arguments, GType provides an id per type instead
#define KNDL_TYPE_SUBCLASS kndl_subclass_get_type ()

G_DECLARE_DERIVABLE_TYPE (KndlSubclass, kndl_subclass, KNDL, SUBCLASS, GObject)

struct _KndlSubclassClass
{
  GObjectClass parent_class;

  void (*pinged) (KndlSubclass*,gint);

  // reserved slots
  void (*_reserved_slot1) (void*);
  void (*_reserved_slot2) (void*);
  void (*_reserved_slot3) (void*);
  void (*_reserved_slot4) (void*);
};

KndlSubclass* kndl_subclass_new (const gchar*); 
GFile*        kndl_subclass_get_file (KndlSubclass*);
void          kndl_subclass_set_file (KndlSubclass*,GFile*);
void          kndl_subclass_ping (KndlSubclass *, gint);

G_END_DECLS
#endif // _KNDL_SUBCLASS_H_
