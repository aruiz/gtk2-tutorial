#include <gtk/gtk.h>

static void
window_closed (GtkWidget *widget,
               GdkEvent  *event,
               gpointer   data) {
  g_message ("%s", (gchar*)data);
  gtk_main_quit ();
}

static void
button_clicked (GtkButton *button) {
  g_message ("Hello world!");
}

gint
main (gint argc, gchar **argv) {
  gtk_init (&argc, &argv);

  GtkWidget *window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (window, "delete-event", G_CALLBACK(window_closed), "Bye bye!");

  GtkWidget *button = gtk_button_new_with_label ("Click me!");

  gtk_container_add (GTK_CONTAINER(window), button);
  g_signal_connect (button, "clicked", G_CALLBACK(button_clicked), NULL);

  gtk_widget_show_all (window);

  gtk_main ();
  return 0;
}
