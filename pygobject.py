from gi.repository import Gtk

def on_click(button, data):
    print("Hey! %d" % data)

win = Gtk.Window(type=Gtk.WindowType.TOPLEVEL, title="PyGObject demo")
button = Gtk.Button(label="Hello world")
button.connect("clicked", on_click, 1)
win.add(button)

win.show_all()
win.connect("destroy", Gtk.main_quit)

Gtk.main()