#include "subclass.h"

static void
pinged_cb (GObject    *self,
           gint        value,
           gpointer    data) {
    g_message ("I was pinged: %d", value);
}

static gboolean
timeout_cb (gpointer data) {
    KndlSubclass *ks1 = KNDL_SUBCLASS (data);
    kndl_subclass_ping (ks1, 101);
    
    return FALSE; // returning FALSE we force the mainloop to remove timeout
}

gint
main (gint argc, gchar **argv) {
    KndlSubclass *ks1 = kndl_subclass_new ("1");

    g_signal_connect (ks1, "pinged", G_CALLBACK(pinged_cb), NULL);

    g_timeout_add (2000, timeout_cb, ks1); // Add a 2 second timeout to the event loop

    // Instantiate and run an event loop
    GMainLoop *loop = g_main_loop_new (NULL, TRUE);
    g_main_loop_run (loop);
    g_object_unref(ks1);
    return 0;
}